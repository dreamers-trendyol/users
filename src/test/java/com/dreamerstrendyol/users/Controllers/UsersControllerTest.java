package com.dreamerstrendyol.users.Controllers;

import com.dreamerstrendyol.users.Contracts.Requests.AddGameDTO;
import com.dreamerstrendyol.users.Contracts.Requests.CreateUserDTO;
import com.dreamerstrendyol.users.Contracts.Requests.UpdateUserDTO;
import com.dreamerstrendyol.users.Contracts.Responses.AccountDetailDTO;
import com.dreamerstrendyol.users.Models.*;
import com.dreamerstrendyol.users.Services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UsersControllerTest {

    @Mock
    private UserService userService;

    private UsersController sut;
    private String userId;

    @BeforeEach
    public void setUp() {
        sut = new UsersController();
        sut.setUserService(userService);
        userId = "123456789";
    }

    @Test
    public void getAllUser_whenCalled_shouldReturnAllUsrListAndOk(){
        //Arrange
        List<User> userList = new ArrayList<>();
        userList.add(new User());
        doReturn(userList).when(userService).getAllUsers(4,5);
        //Act
        ResponseEntity<List<User>> result = sut.getAllUsers(4,5);
        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void getUserById_whenCalledWithId_shouldReturnOkAndUser() throws Exception {
        //Arrange
        User user = new User();
        user.setUserId(userId);
        doReturn(user).when(userService).getUserById(userId);

        //Act
        ResponseEntity result = sut.getUserById(userId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void createUser_whenCalled_shouldReturnUrlWithUserId(){
        //Arrange
        URI url = URI.create(userId);
        CreateUserDTO createUserDTO = new CreateUserDTO();
        doReturn(url.toString()).when(userService).createUser(createUserDTO);


        //Act
        ResponseEntity result = sut.createUser(createUserDTO);

        //Verify
        assertThat(result.getHeaders().getLocation().toString()).isEqualTo(url.toString());
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    public void editUser_whenCalled_shouldReturnNoContent(){
        //Arrange
        UpdateUserDTO updateUserDTO = new UpdateUserDTO();
        doNothing().when(userService).updateUser(userId,null);

        //Act
        ResponseEntity result = sut.editUser(userId,updateUserDTO);

        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void getUserAccountDetail_whenCalled_shouldReturnUserAccountDetailAndOk(){
        //Arrange
        String userName = "abcdef";
        AccountDetailDTO accountDetailDTO = new AccountDetailDTO();
        accountDetailDTO.setUsername(userName);
        doReturn(accountDetailDTO).when(userService).getUserAccountDetails(userId);
        //Act
        ResponseEntity<AccountDetailDTO> result = sut.getUserAccountDetail(userId);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody().getUsername()).isEqualTo(userName);
    }

    @Test
    public void setUserPasswordResetted_whenCalled_shouldReturnNoContent(){
        //Arrange
        doNothing().when(userService).resetPassword(userId,null);

        //Act
        ResponseEntity result = sut.setUserPasswordResetted(userId,null);

        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void setUserPassword_whenCalled_shouldReturnNoContent(){
        //Arrange
        doNothing().when(userService).changePassword(userId,null);

        //Act
        ResponseEntity result = sut.setUserPassword(userId,null);

        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void getUserProfileComments_whenCalled_shouldReturnAllUserCommentList(){
        //Arrange
        List<UserComment> userCommentList = new ArrayList<>();
        UserComment userComment = new UserComment();
        userComment.setComment("abc comment");
        userComment.setUserId(userId);
        doReturn(userCommentList).when(userService).getUserProfileComments(userId);
        //Act
        ResponseEntity<List<UserComment>> result = sut.getUserProfileComments(userId);

        //Verify

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody().size()).isEqualTo(userCommentList.size());
    }

    @Test
    public void addProfileCommentToUser_whenCalled_shouldReturnOk(){
        //Arrange
        UserComment userComment = new UserComment();
        userComment.setComment("abc comment");
        userComment.setUserId(userId);
        doNothing().when(userService).addProfileCommentToUser(userId,userComment);
        //Act
        ResponseEntity result = sut.addProfileCommentToUser(userId,userComment);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void deleteProfileCommentToUser_whenCalled_shouldReturnOk(){
        //Arrange
        UserComment userComment = new UserComment();
        userComment.setComment("abc comment");
        userComment.setUserId(userId);
        doNothing().when(userService).deleteProfileCommentToUser(userId,userComment);
        //Act
        ResponseEntity result = sut.deleteProfileCommentToUser(userId,userComment);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void getUserFriends_whenCalled_shouldReturnUserFriendsByUserId(){
        //Arrange
        List<Friend> friendList = new ArrayList<>();
        friendList.add(new Friend());
        friendList.add(new Friend());
        doReturn(friendList).when(userService).getUserFriends(userId);
        //Act
        ResponseEntity<List<Friend>> result = sut.getUserFriends(userId);

        //Verify

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody().size()).isEqualTo(friendList.size());
    }

    @Test
    void addFriendToUser_whenCalled_shouldReturnOk() {
        //Arrange
        Friend friend = new Friend();
        friend.setUserId("abc");
        doNothing().when(userService).addFriendToUser(userId,friend);
        //Act
        ResponseEntity result = sut.addFriendToUser(userId,friend);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void deleteFriendFromUser_whenCalled_shouldReturnOk() {
        //Arrange
        Friend friend = new Friend();
        friend.setUserId("abc");
        doNothing().when(userService).deleteFriendFromUser(userId,friend);
        //Act
        ResponseEntity result = sut.deleteFriendFromUser(userId,friend);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void addWishToWishlist_whenCalled_shouldReturnOk() {
        //Arrange
        doNothing().when(userService).addWishToWishList(userId,"1234");
        //Act
        ResponseEntity result = sut.addWishToWishlist(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void deleteWishToWishlist_whenCalled_shouldReturnOk() {
        //Arrange
        doNothing().when(userService).deleteWishToWishList(userId,"12345");
        //Act
        ResponseEntity result = sut.deleteWishToWishlist(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void getUserMedias_whenCalled_shouldReturnUserMediaListAndOk() {
        //Arrange
        List<MediaItem> mediaItemList = new ArrayList<>();
        mediaItemList.add(new MediaItem("/url/media"));
        doReturn(mediaItemList).when(userService).getUserMedias(userId);
        //Act
        ResponseEntity<List<MediaItem>> result = sut.getUserMedias(userId);
        //Verify
        assertThat(result.getBody().size()).isEqualTo(mediaItemList.size());
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void addUserMedia_whenCalled_shouldReturnOk() {
        //Arrange
        doNothing().when(userService).addUserMedia(userId,"/url/media");
        //Act
        ResponseEntity result = sut.addUserMedia(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void deleteUserMedia_whenCalled_shouldReturnOk() {
        //Arrange
        doNothing().when(userService).deleteUserMedia(userId,"/url/media");
        //Act
        ResponseEntity result = sut.deleteUserMedia(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void getUserInventory_whenCalled_shouldReturnUserInventoryByIdAndOk() {
        //Arrange
        List<Item> itemList = new ArrayList<>();
        itemList.add(new Item());
        doReturn(itemList).when(userService).getUserInventory(userId);
        //Act
        ResponseEntity<List<Item>> result = sut.getUserInventory(userId);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody().size()).isEqualTo(itemList.size());
    }

    @Test
    void addItemToUser_whenCAlled_shouldReturnNoContent() {
        //Arrange
        Item item = new Item();
        doNothing().when(userService).addItemToUserInventory(userId,item);
        //Act
        ResponseEntity result = sut.addItemToUser(userId,item);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    void deleteItemFromUser_whenCalled_shouldReturnNoContent() {
        //Arrange
        doNothing().when(userService).deleteItemFromUser(userId,"12345");
        //Act
        ResponseEntity result = sut.deleteItemFromUser(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    void getUserItemById_whenCalled_shouldReturnUserItemByIdAndOk() {
        //Arrange
        Item item = new Item();
        item.setItemId("12345");
        doReturn(item).when(userService).getUserItemById(userId,item.getItemId());
        //Act
        ResponseEntity<Item> result = sut.getUserItemById(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        //assertThat(result.getBody().getItemId()).isEqualTo(item.getItemId());
    }

    @Test
    void getUserGames_whenCalled_shouldReturnUserGamesAndOk() {
        //Arrange
        List<Game> gameList = new ArrayList<>();
        gameList.add(new Game());
        doReturn(gameList).when(userService).getUserGames(userId);
        //Act
        ResponseEntity<List<Game>> result = sut.getUserGames(userId);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody().size()).isEqualTo(gameList.size());
    }

    @Test
    void getUserGameById_whenCalled_shouldReturnUserGameByIdAndOk() {
        //Arrange
        Game game = new Game("abcd","AC");
        doReturn(game).when(userService).getUserGameById(userId,game.getGameId());
        //Act
        ResponseEntity<Game> result = sut.getUserGameById(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        //assertThat(result.getBody().getGameName()).isEqualTo(game.getGameName());
    }

    @Test
    void addGameToUser_whenCalled_shouldReturnOk() {
        //Arrange
        AddGameDTO addGameDTO = new AddGameDTO();
        doNothing().when(userService).addGameToUser(userId,addGameDTO);
        //Act
        ResponseEntity result = sut.addGameToUser(userId,addGameDTO);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void deleteGameFromUser_whenCalled_shouldReturnOk() {
        //Arrange
        doNothing().when(userService).deleteGameFromUser(userId,"gameId");
        //Act
        ResponseEntity result = sut.deleteGameFromUser(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void makeUserInGame_whenCalled_shouldReturnOk() {
        //Arrange
        doNothing().when(userService).userJoinedGame(userId,"gameId");
        //Act
        ResponseEntity result = sut.makeUserInGame(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void makeUserExitFromGame_whenCalled_shouldReturnOk() {
        //Arrange
        doNothing().when(userService).userExitGame(userId,"gameId");
        //Act
        ResponseEntity result = sut.makeUserExitFromGame(userId,null);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void getUserBalance_whenCalled_shouldReturnUserBalanceAndOk() {
        //Arrange
        Double balance=29.99;
        doReturn(balance).when(userService).getUserBalance(userId);
        //Act
        ResponseEntity<Double> result = sut.getUserBalance(userId);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(balance);
    }

    @Test
    void increaseBalanceOfUser_whenCalled_shouldReturnOk() {
        //Arrange
        doNothing().when(userService).increaseBalanceOfUser(userId,25.12);
        //Act
        ResponseEntity result = sut.increaseBalanceOfUser(userId,0);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void decreaseBalanceOfUser_whenCalled_shouldReturnOk() {
        //Arrange
        doNothing().when(userService).decreaseBalanceOfUser(userId,25.12);
        //Act
        ResponseEntity result = sut.decreaseBalanceOfUser(userId,0);
        //Verify
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
