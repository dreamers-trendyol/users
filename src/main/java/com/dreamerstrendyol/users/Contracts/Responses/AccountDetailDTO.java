package com.dreamerstrendyol.users.Contracts.Responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountDetailDTO {
    private String username;
    private String email;
    private String avatarUrl;
    private String about;
}