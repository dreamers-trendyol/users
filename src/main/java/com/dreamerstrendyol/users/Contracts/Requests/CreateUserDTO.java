package com.dreamerstrendyol.users.Contracts.Requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserDTO {
    @NotNull
    @Length(min = 3, max = 70)
    private String username;
    @NotNull
    @Length(min = 8, max = 70)
    private String password;
    @NotNull
    @Length(min = 3, max = 70)
    @Email
    private String email;
    @NotNull
    @Length(min = 3, max = 250)
    private String avatarUrl;
    @NotNull
    @Length(min = 3, max = 250)
    private String about;
}
