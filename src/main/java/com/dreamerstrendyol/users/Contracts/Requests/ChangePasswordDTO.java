package com.dreamerstrendyol.users.Contracts.Requests;

import lombok.Getter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
public class ChangePasswordDTO {
    @NotNull
    @Length(min = 8, max = 70)
    private String oldPassword;
    @NotNull
    @Length(min = 8, max = 70)
    private String newPassword;
    @NotNull
    @Email
    @Length(min = 3, max = 70)
    private String email;
}
