package com.dreamerstrendyol.users.Contracts.Requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AddGameDTO {
    @NotNull
    @Length(min=3,max=36)
    String gameId;

    @NotNull
    @Length(min=3,max=70)
    String gameName;

}
