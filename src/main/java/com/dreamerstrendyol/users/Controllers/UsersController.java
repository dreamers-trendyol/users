package com.dreamerstrendyol.users.Controllers;

import com.dreamerstrendyol.users.Contracts.Requests.AddGameDTO;
import com.dreamerstrendyol.users.Contracts.Requests.ChangePasswordDTO;
import com.dreamerstrendyol.users.Contracts.Requests.CreateUserDTO;
import com.dreamerstrendyol.users.Contracts.Requests.UpdateUserDTO;
import com.dreamerstrendyol.users.Contracts.Responses.AccountDetailDTO;
import com.dreamerstrendyol.users.Helpers.PatchHelper;
import com.dreamerstrendyol.users.Models.*;
import com.dreamerstrendyol.users.Services.UserService;
import com.github.fge.jsonpatch.JsonPatch;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/user")
public class UsersController {


    @Setter(AccessLevel.PACKAGE)
    @Autowired
    UserService userService;

    //DONE
    @GetMapping("/")
    public ResponseEntity getAllUsers(@RequestParam(required = false, defaultValue = "1") int page, @RequestParam(required = false, defaultValue = "10") int limit) {
        return ResponseEntity.ok(userService.getAllUsers((page - 1) * limit, limit));
    }
    //DONE
    @PostMapping("/")
    public ResponseEntity createUser(@Valid @RequestBody CreateUserDTO createUserDTO) {
        String userId = userService.createUser(createUserDTO);
        return ResponseEntity.created(URI.create(userId)).build();
    }

    //DONE
    @GetMapping("/{userId}")
    public ResponseEntity getUserById(@PathVariable String userId) {
        User user = userService.getUserById(userId);
        return ResponseEntity.ok(user);
    }


    //DONE
    @PatchMapping("/{userId}")
    public ResponseEntity editUser(@PathVariable String userId, @RequestBody UpdateUserDTO updatePatch) {

        try {
            JsonPatch patch = PatchHelper.createJsonPatch(updatePatch);
            userService.updateUser(userId, patch);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return ResponseEntity.noContent().build();

    }

    ////
    // ACCOUNT
    ////
    @GetMapping("/{userId}/account")
    public ResponseEntity getUserAccountDetail(@PathVariable String userId) {
        AccountDetailDTO accountDetailDTO = userService.getUserAccountDetails(userId);
        return ResponseEntity.ok(accountDetailDTO);
    }

    @PostMapping("/{userId}/account/reset-password")
    public ResponseEntity setUserPasswordResetted(@PathVariable String userId, @RequestBody String email) {
        userService.resetPassword(userId, email);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{userId}/account/change-password")
    public ResponseEntity setUserPassword(@PathVariable String userId, @Valid @RequestBody ChangePasswordDTO changePasswordDTO) {
        userService.changePassword(userId, changePasswordDTO);
        return ResponseEntity.noContent().build();
    }

    ////
    // COMMENTS
    ////
    @GetMapping("/{userId}/profile-comments")
    public ResponseEntity getUserProfileComments(@PathVariable String userId) {
        List<UserComment> userComments = userService.getUserProfileComments(userId);
        return ResponseEntity.ok(userComments);
    }

    @PostMapping("/{userId}/profile-comments")
    public ResponseEntity addProfileCommentToUser(@PathVariable String userId, @Valid @RequestBody UserComment userComment) {
        userService.addProfileCommentToUser(userId, userComment);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{userId}/profile-comments")
    public ResponseEntity deleteProfileCommentToUser(@PathVariable String userId, @Valid @RequestBody UserComment userComment) {
        userService.deleteProfileCommentToUser(userId, userComment);
        return ResponseEntity.ok().build();
    }

    ////
    // FRIENDS
    ////

    //DONE
    @GetMapping("/{userId}/friends")
    public ResponseEntity getUserFriends(@PathVariable String userId) {
        List<Friend> friends = userService.getUserFriends(userId);
        return ResponseEntity.ok(friends);
    }

    //DONE
    @PostMapping("/{userId}/friends")
    public ResponseEntity addFriendToUser(@PathVariable String userId, @Valid @RequestBody Friend friend) {
        userService.addFriendToUser(userId, friend);
        return ResponseEntity.ok().build();
    }

    //DONE
    @DeleteMapping("/{userId}/friends")
    public ResponseEntity deleteFriendFromUser(@PathVariable String userId, @Valid @RequestBody Friend friend) {
        userService.deleteFriendFromUser(userId, friend);
        return ResponseEntity.ok().build();
    }

    ////
    // Wishlist
    //
    ////

    @PostMapping("/{userId}/wishlist")
    public ResponseEntity addWishToWishlist(@PathVariable String userId, @RequestBody String gameId) {
        userService.addWishToWishList(userId, gameId);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{userId}/wishlist/{gameId}")
    public ResponseEntity deleteWishToWishlist(@PathVariable String userId, @PathVariable String gameId) {
        userService.deleteWishToWishList(userId, gameId);
        return ResponseEntity.ok().build();
    }
    ////
    // MEDIA
    // b64 maybe
    ////

    @GetMapping("/{userId}/media")
    public ResponseEntity getUserMedias(@PathVariable String userId) {
        List<MediaItem> medias = userService.getUserMedias(userId);
        return ResponseEntity.ok(medias);
    }

    @PostMapping("/{userId}/media")
    public ResponseEntity addUserMedia(@PathVariable String userId, @RequestBody String mediaUrl) {
        userService.addUserMedia(userId, mediaUrl);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{userId}/media/{mediaUrl}")
    public ResponseEntity deleteUserMedia(@PathVariable String userId, @PathVariable String mediaUrl) {
        userService.deleteUserMedia(userId, mediaUrl);
        return ResponseEntity.ok().build();
    }

    //////
    //Inventory
    //////

    @GetMapping("/{userId}/inventory")
    public ResponseEntity getUserInventory(@PathVariable String userId) {
        List<Item> inventory = userService.getUserInventory(userId);
        return ResponseEntity.ok(inventory);
    }

    @PostMapping("/{userId}/inventory")
    public ResponseEntity addItemToUser(@PathVariable String userId, @RequestBody Item item) {
        userService.addItemToUserInventory(userId, item);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{userId}/inventory/{itemId}")
    public ResponseEntity deleteItemFromUser(@PathVariable String userId, @PathVariable String itemId) {
        userService.deleteItemFromUser(userId, itemId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{userId}/inventory/{itemId}")
    public ResponseEntity getUserItemById(@PathVariable String userId, @PathVariable String itemId) {
        Item item = userService.getUserItemById(userId, itemId);
        return ResponseEntity.ok(item);
    }
    ////
    // GAMES
    ////

    @GetMapping("/{userId}/games")
    public ResponseEntity getUserGames(@PathVariable String userId) {
        List<Game> games = userService.getUserGames(userId);
        return ResponseEntity.ok(games);
    }

    @GetMapping("/{userId}/games/{gameId}")
    public ResponseEntity getUserGameById(@PathVariable String userId, @PathVariable String gameId) {
        Game gameById = userService.getUserGameById(userId, gameId);
        return ResponseEntity.ok(gameById);
    }

    @PostMapping("/{userId}/games")
    public ResponseEntity addGameToUser(@PathVariable String userId, @Valid @RequestBody AddGameDTO addGameDTO) {
        userService.addGameToUser(userId, addGameDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{userId}/games/{gameId}")
    public ResponseEntity deleteGameFromUser(@PathVariable String userId, @PathVariable String gameId) {
        userService.deleteGameFromUser(userId, gameId);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{userId}/games/{gameId}/join")
    public ResponseEntity makeUserInGame(@PathVariable String userId, @PathVariable String gameId) {
        userService.userJoinedGame(userId, gameId);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{userId}/games/{gameId}/exit")
    public ResponseEntity makeUserExitFromGame(@PathVariable String userId, @PathVariable String gameId) {
        userService.userExitGame(userId, gameId);
        return ResponseEntity.ok().build();
    }

    ////
    // BALANCE
    ////

    //DONE
    @GetMapping("/{userId}/balance")
    public ResponseEntity getUserBalance(@PathVariable String userId) {
        double balance = userService.getUserBalance(userId);
        return ResponseEntity.ok(balance);
    }

    //DONE
    @PostMapping("/{userId}/balance/")
    public ResponseEntity increaseBalanceOfUser(@PathVariable String userId, @RequestBody double theBalanceWillAdded) {
        userService.increaseBalanceOfUser(userId, theBalanceWillAdded);
        return ResponseEntity.ok().build();
    }

    //DONE
    @DeleteMapping("/{userId}/balance/")
    public ResponseEntity decreaseBalanceOfUser(@PathVariable String userId, @RequestBody double theBalanceWillRemoved) {
        userService.decreaseBalanceOfUser(userId, theBalanceWillRemoved);
        return ResponseEntity.ok().build();
    }
}
