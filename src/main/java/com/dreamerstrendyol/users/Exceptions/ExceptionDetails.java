package com.dreamerstrendyol.users.Exceptions;

import lombok.Getter;

import java.util.Date;

@Getter
public class ExceptionDetails {
    private String errorType;
    private String errorMessage;
    private String requestInfo;
    private Date time;

    public ExceptionDetails(String errorType, String errorMessage, String requestInfo, Date time) {
        this.errorType = errorType;
        this.errorMessage = errorMessage;
        this.requestInfo = requestInfo;
        this.time = time;
    }
}