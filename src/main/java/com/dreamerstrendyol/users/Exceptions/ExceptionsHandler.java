package com.dreamerstrendyol.users.Exceptions;

import com.couchbase.client.core.error.CasMismatchException;
import com.couchbase.client.core.error.DocumentNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;
import java.util.NoSuchElementException;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<Object> nullPointerException(NullPointerException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("NullPointerException",
                "You trying to make request null pointer target",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({NoSuchElementException.class})
    public ResponseEntity<Object> noSuchElementException(NoSuchElementException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("NoSuchElementException",
                "Server can't find the request id.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({CasMismatchException.class})
    public ResponseEntity<Object> documentNotFoundException(CasMismatchException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("CasMismatchException",
                "Document has been concurrently modified on the server.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({DocumentNotFoundException.class})
    public ResponseEntity<Object> documentNotFoundException(DocumentNotFoundException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("DocumentNotFoundException",
                "There is no document as identified with this id.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<Object> httpMessageNotReadableException(HttpMessageNotReadableException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("HttpMessageNotReadableException",
                "Request can't be accepted.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ResourceAccessException.class})
    public ResponseEntity<Object> resourceAccessException(ResourceAccessException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("ResourceAccessException",
                "Server can't access data please try again.",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<Object> methodArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("MethodArgumentNotValidException",
                "Some Fields need to ",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({HttpMediaTypeNotSupportedException.class})
    public ResponseEntity<Object> httpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("HttpMediaTypeNotSupportedException",
                "Please try to change request header as application/JSON",
                request.getDescription(true), new Date());
        return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
    }
}
