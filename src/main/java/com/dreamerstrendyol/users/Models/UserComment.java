package com.dreamerstrendyol.users.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserComment {
    @NotNull
    @Length(min=3,max=36)
    private String userId;
    @NotNull
    @Length(min=3,max=250)
    private String comment;
    private String createdAt;
}
