package com.dreamerstrendyol.users.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Friend {
    @NotNull
    @Length(min=3,max=36)
    private String userId;
    private String createdAt;
}
