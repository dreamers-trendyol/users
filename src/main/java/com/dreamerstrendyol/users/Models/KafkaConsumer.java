package com.dreamerstrendyol.users.Models;

import com.dreamerstrendyol.users.Services.MessageQueueKafkaServiceImpl;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Getter
@Component
public class KafkaConsumer {

    @Autowired
    MessageQueueKafkaServiceImpl messageQueueKafkaServiceImpl;

    @KafkaListener(topics = "userBuyGame")
    void userTryBuyGameListener(String data) {
        messageQueueKafkaServiceImpl.userBuyGame(data);
    }

    @KafkaListener(topics = "announcement")
    public void announcementListener(String data) {
        messageQueueKafkaServiceImpl.createAnnouncement(data);
    }

    @KafkaListener(topics = "userDeleteListedItem")
    public void userDeleteListedItem(String data) {
        messageQueueKafkaServiceImpl.userDeleteListedItem(data);
    }

    @KafkaListener(topics = "userListedItem")
    public void userListedItem(String data) {
        messageQueueKafkaServiceImpl.userListedItem(data);
    }

    @KafkaListener(topics = "userBoughtAnItem")
    public void userBoughtAnItem(String data) {
        messageQueueKafkaServiceImpl.userBoughtAnItem(data);
    }

    @KafkaListener(topics = "discountGame")
    public void priceDecreasedNotification(String data) {
        messageQueueKafkaServiceImpl.priceDecreasedNotification(data);
    }

}


