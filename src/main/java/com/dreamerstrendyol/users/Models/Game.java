package com.dreamerstrendyol.users.Models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class Game {
    private String gameId;
    private String gameName;
    private int userInGameMinutes;
    private String lastTimePlayed;
    private String firstTimePlayed;

    public Game(String gameId, String gameName) {
        this.gameId = gameId;
        this.gameName = gameName;
        this.userInGameMinutes = 0;
        this.lastTimePlayed = "";
        this.firstTimePlayed = "";
    }

    public void updateGameLastTimePlayed() {
        if (this.firstTimePlayed.equals("")) {
            this.firstTimePlayed = LocalDate.now().toString();
        }
        this.lastTimePlayed = LocalDate.now().toString();
    }

    public void increaseInGameMinutes(int minute) {
        this.userInGameMinutes += minute;
    }
}
