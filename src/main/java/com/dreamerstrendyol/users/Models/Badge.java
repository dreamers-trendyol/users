package com.dreamerstrendyol.users.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Badge {
    @NotNull
    private String badgeIconUrl;
    @NotNull
    private String badgeName;
    @NotNull
    private String badgeDescription;
}
