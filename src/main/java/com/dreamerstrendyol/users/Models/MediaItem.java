package com.dreamerstrendyol.users.Models;

import lombok.Getter;
import java.time.LocalDate;

@Getter
public class MediaItem {
    private String mediaUrl;
    private String createdAt;

    public MediaItem(String mediaUrl) {
        this.mediaUrl = mediaUrl;
        this.createdAt= LocalDate.now().toString();
    }
}
