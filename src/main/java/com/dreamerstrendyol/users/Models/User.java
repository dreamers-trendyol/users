package com.dreamerstrendyol.users.Models;

import com.dreamerstrendyol.users.Validators.PasswordHash;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String userId;
    private String username;
    private String password;
    private String email;
    private String avatarUrl;
    private String about;
    private List<Game> games;
    private List<Item> inventory;
    private List<Friend> friends;
    private List<UserComment> profileComments;
    private List<MediaItem> medias;
    private List<Badge> badges;
    private List<Wish> wishList;
    private double balance;


    public User(String username, String password, String email, String avatarUrl, String about) {
        this.userId = UUID.randomUUID().toString();
        this.username = username;
        this.password = new PasswordHash().hashPassword(password);
        this.email = email;
        this.avatarUrl = avatarUrl;
        this.about = about;
        this.games = new ArrayList<Game>();
        this.inventory = new ArrayList<Item>();
        this.friends = new ArrayList<Friend>();
        this.profileComments = new ArrayList<UserComment>();
        this.medias = new ArrayList<MediaItem>();
        this.badges = new ArrayList<Badge>();
        this.balance = 0.0;
    }

    public void addItemToInventory(Item item) {
        this.inventory.add(item);
    }

    public void deleteItemToInventory(String itemId) {
        Item item = getItem(itemId);
        inventory.remove(item);
    }

    public Item getItem(String itemId) {
        Item theItem = inventory.stream()
                .filter(i -> i.getItemId().equals(itemId))
                .findFirst()
                .get();
        return theItem;
    }

    public void setItemListedOnMerchantStatus(String itemId, boolean status) {
        inventory.stream()
                .filter(i -> i.getItemId().equals(itemId))
                .findFirst()
                .get().setCanListedAtUserMerchant(status);
    }

    public void addMedia(String mediaUrl) {
        MediaItem mediaItem = new MediaItem(mediaUrl);
        this.medias.add(mediaItem);
    }

    public void deleteMedia(String mediaUrl) {
        MediaItem theMedia = medias.stream()
                .filter(m -> m.getMediaUrl().equals(mediaUrl))
                .findFirst()
                .get();
        medias.remove(theMedia);
    }

    public void addFriend(Friend friend) {
        this.friends.add(friend);
        int friendSize = friends.size();
        if (friendSize % 5 == 0) {
            addGamerHasLotsFriendsBadge(friendSize);
        }
    }

    public void deleteFriend(Friend friend) {
        this.friends.remove(friend);
    }

    public void addProfileComment(UserComment userComment) {
        this.profileComments.add(userComment);
        int commentNumber = friends.size();
        if (commentNumber % 15 == 0) {
            addGoingPopularBadge(commentNumber);
        }
    }

    public void deleteProfileComment(UserComment userComment) {
        this.profileComments.remove(userComment);
    }

    public void increaseBalance(double value) {
        double userBalance = this.getBalance();
        this.setBalance(userBalance + Math.abs(value));

        if (value >= 250) {
            addGameIndustryHelperBadge();
        }
    }

    public void decreaseBalance(double value) {
        double userBalance = this.getBalance();
        this.setBalance(userBalance - Math.abs(value));
    }

    public void addGameToUser(String gameId, String gameName) {
        Game game = new Game(gameId, gameName);
        this.games.add(game);

        int gameSize = games.size();
        if (gameSize % 5 == 0) {
            addFirstStepBadge(gameSize);
        }

    }

    public void deleteGame(String gameId) {
        Game game = getUserGameById(gameId);
        games.remove(game);
    }

    public Game getUserGameById(String gameId) {
        Game game = getGames().stream()
                .filter(g -> g.getGameId().equals(gameId))
                .findFirst()
                .get();
        return game;
    }

    private void addFirstStepBadge(int gameNumber) {
        Badge badge = new Badge("url", "First step to gaming",
                "Wow You bought  " + gameNumber + " games.");
        this.badges.add(badge);
    }

    private void addGamerHasLotsFriendsBadge(int friendNumber) {
        Badge badge = new Badge("url", "Gamer friends around",
                "Wow You have " + friendNumber + " friends to play.");
        this.badges.add(badge);
    }

    private void addGoingPopularBadge(int commentNumber) {
        Badge badge = new Badge("url", "You are going popular",
                "Woow You have " + commentNumber + " commets.");
        this.badges.add(badge);
    }

    private void addGameIndustryHelperBadge() {
        Badge badge = new Badge("url", "Game Industry Helper",
                "Thank you. You are a big supporter!!");
        this.badges.add(badge);
    }

    public void addWishToList(String gameId) {
        this.wishList.add(new Wish(gameId, LocalDate.now().toString()));
    }

    public void deleteWishFromList(String gameId) {
            Wish wish = wishList.stream()
                    .filter(i -> i.getGameId().equals(gameId))
                    .findFirst()
                    .get();
            wishList.remove(wish);
    }
}
