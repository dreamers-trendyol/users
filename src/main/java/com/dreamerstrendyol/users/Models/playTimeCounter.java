package com.dreamerstrendyol.users.Models;

import lombok.Getter;

import java.util.Timer;
import java.util.TimerTask;

@Getter
public class playTimeCounter {
    private int playTime = 0;
    private String userId;
    private String gameId;
    private Timer timer;

    public playTimeCounter(String userId, String gameId) {
        this.userId = userId;
        this.gameId = gameId;
        TimerTask task = new TimerTask() {
            public void run() {
                playTime += 1;
                System.out.println("basladi"+playTime);
            }
        };
        Timer timer = new Timer("Timer");

        timer.schedule(task, 0, 1000);
        this.timer = timer;
    }

    public void kill() {
        this.timer.cancel();
    }
}

