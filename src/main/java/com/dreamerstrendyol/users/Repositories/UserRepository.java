package com.dreamerstrendyol.users.Repositories;

import com.dreamerstrendyol.users.Models.Friend;
import com.dreamerstrendyol.users.Models.User;

import java.util.List;

public interface UserRepository {
    void createUser(User user);
    User getUserById(String userId);
    List<Friend> getUserFriends(String userId);
    void updateUser(User user);
    List<User> getUsersHaveGameWithGameId(String gameId);
    List<User> getAllUsers(int offset, int limit);
    List<User> getUsersHaveTheGameInWishList(String gameId);
}
