package com.dreamerstrendyol.users.Repositories;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.json.JsonObject;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.query.QueryResult;
import com.dreamerstrendyol.users.Models.Friend;
import com.dreamerstrendyol.users.Models.User;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.couchbase.client.java.query.QueryOptions.queryOptions;

@Repository
public class UsersRepositoryImpl implements UserRepository {
    @Autowired
    @Setter(AccessLevel.PACKAGE)
    private Cluster couchbaseCluster;

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    private Collection usersCollection;

    @Override
    public void createUser(User user) {
        usersCollection.insert(user.getUserId(), user);
    }

    @Override
    public User getUserById(String userId) {
        GetResult getResult = usersCollection.get(userId);
        return getResult.contentAs(User.class);
    }

    @Override
    public List<Friend> getUserFriends(String userId) {
        GetResult getResult = usersCollection.get(userId);
        User user = getResult.contentAs(User.class);
        return user.getFriends();
    }

    @Override
    public void updateUser(User user) {
        usersCollection.replace(user.getUserId(), user);
    }

    @Override
    public List<User> getUsersHaveGameWithGameId(String gameId) {
        String statement = "SELECT email FROM users WHERE ANY game IN users.games SATISFIES game.gameId=$gameId end";
        QueryResult query = couchbaseCluster.query(statement,
                queryOptions().parameters(JsonObject.create().put("gameId", gameId)));
        return query.rowsAs(User.class);
    }

    @Override
    public List<User> getAllUsers(int offset, int limit) {
        String statement = String.format("SELECT about,avatarUrl,badges,balance,email,friends,games,inventory,medias,profileComments,userId,username,wishList FROM users OFFSET %d LIMIT %d", offset, limit);
        QueryResult queryResult = couchbaseCluster.query(statement);
        return queryResult.rowsAs(User.class);
    }

    @Override
    public List<User> getUsersHaveTheGameInWishList(String gameId) {
        String statement = "SELECT email FROM users WHERE ANY wish IN users.wishList SATISFIES wish.gameId=$gameId end";
        QueryResult query = couchbaseCluster.query(statement,
                queryOptions().parameters(JsonObject.create().put("gameId", gameId)));
        return query.rowsAs(User.class);
    }
}
