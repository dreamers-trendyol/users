package com.dreamerstrendyol.users.Validators;

import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

public class PasswordHash {

    Pbkdf2PasswordEncoder encoder = new Pbkdf2PasswordEncoder("secret", 10000, 128);

    //   boolean murat_turan = encoder.matches("Murat TURAN", muratTURAN);
    public String hashPassword(String userPassword) {
        return encoder.encode(userPassword);
    }

    public boolean hashPasswordMatcher(String rawUserPassword, String userPassword) {
        return encoder.matches(rawUserPassword, userPassword);
    }
}
