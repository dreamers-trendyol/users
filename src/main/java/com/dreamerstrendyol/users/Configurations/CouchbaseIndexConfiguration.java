package com.dreamerstrendyol.users.Configurations;

import com.couchbase.client.java.Cluster;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CouchbaseIndexConfiguration {

    private final Cluster couchbaseCluster;

    public CouchbaseIndexConfiguration(Cluster couchbaseCluster) {
        this.couchbaseCluster = couchbaseCluster;
    }

    @Bean
    public void createIndexes() {
       try{
           couchbaseCluster.query("CREATE INDEX email ON users(email);");
           couchbaseCluster.query("CREATE INDEX username ON users(username);");
           couchbaseCluster.query("CREATE INDEX userId ON users(userId);");
           couchbaseCluster.query("CREATE INDEX inventory ON users(inventory);");
           couchbaseCluster.query("CREATE INDEX balance ON users(balance);");
           couchbaseCluster.query("CREATE INDEX games ON users(games);");
       }catch (Exception e){
           System.out.println(e.getMessage());
       }
    }
}


