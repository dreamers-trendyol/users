package com.dreamerstrendyol.users.Helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JacksonUtils;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;

public class PatchHelper {

    public static <T2 extends Object> JsonPatch createJsonPatch(T2 destEntity) throws IllegalAccessException {
        JSONObject jsonObject=null;
        JSONArray jsonArray = new JSONArray();
        Field[] fields = destEntity.getClass().getDeclaredFields();
        for (Field field : fields) {
            if(field.get(destEntity) != null && !field.get(destEntity).equals(0.0)) {
                jsonObject = new JSONObject();
                jsonObject.put("op","replace");
                jsonObject.put("path","/"+field.getName());
                jsonObject.put("value",field.get(destEntity));
                System.out.println(jsonObject.toString());
                jsonArray.put(jsonObject);
            }
        }
        JsonPatch patch = null;
        try {
            patch = JsonPatch.fromJson(JacksonUtils.getReader().readTree(jsonArray.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return patch;
    }
    public static  <T extends Object> T applyPatch(JsonPatch patch, T target) throws
            JsonPatchException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        JsonNode patched = patch.apply(objectMapper.convertValue(target, JsonNode.class));
        return (T) objectMapper.treeToValue(patched, target.getClass());
    }
}
