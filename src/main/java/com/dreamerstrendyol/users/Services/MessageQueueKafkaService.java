package com.dreamerstrendyol.users.Services;

public interface MessageQueueKafkaService {
    void createAnnouncement(String data);
    void userBuyGame(String data);
    void userListedItem(String data);
    void userBoughtAnItem(String data);
    void userDeleteListedItem(String data);
    void priceDecreasedNotification(String data);
}
