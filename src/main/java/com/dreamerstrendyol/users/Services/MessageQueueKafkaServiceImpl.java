package com.dreamerstrendyol.users.Services;

import com.dreamerstrendyol.users.Models.Item;
import com.dreamerstrendyol.users.Models.KafkaSender;
import com.dreamerstrendyol.users.Models.User;
import com.dreamerstrendyol.users.Repositories.UserRepository;
import lombok.AccessLevel;
import lombok.Setter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageQueueKafkaServiceImpl implements MessageQueueKafkaService {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    UserRepository userRepository;

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    KafkaSender sender;

    @Override
    public void createAnnouncement(String data) {
        JSONObject announcement = new JSONObject(data);
        List<User> usersHaveTheGame = userRepository.getUsersHaveGameWithGameId(announcement.getString("gameId"));
        for (User user : usersHaveTheGame) {
            //send mail user.getEmail()
        }
    }

    @Override
    public void userBuyGame(String data) {
        JSONObject jsonObject = new JSONObject(data);
        User userById = userRepository.getUserById(jsonObject.getString("userId"));
        userById.addGameToUser(jsonObject.getString("gameId"), jsonObject.getString("gameName"));
        userById.decreaseBalance(jsonObject.getDouble("price"));
        userRepository.updateUser(userById);
    }

    @Override
    public void userListedItem(String data) {
        JSONObject jsonObject = new JSONObject(data);
        User sellerUser = userRepository.getUserById(jsonObject.getString("userId"));
        sellerUser.setItemListedOnMerchantStatus(jsonObject.getString("itemId"), false);
        userRepository.updateUser(sellerUser);
        System.out.println(data);
    }

    @Override
    public void userBoughtAnItem(String data) {
        JSONObject jsonObject = new JSONObject(data);
        User buyerById = userRepository.getUserById(jsonObject.getString("userId"));
        User ownerById = userRepository.getUserById(jsonObject.getString("ownerUserId"));

        ownerById.deleteItemToInventory(jsonObject.getString("itemId"));
        buyerById.addItemToInventory(new Item(jsonObject.getString("itemId"), true));

        ownerById.increaseBalance(jsonObject.getDouble("itemPrice"));
        buyerById.decreaseBalance(jsonObject.getDouble("itemPrice"));

        userRepository.updateUser(buyerById);
        userRepository.updateUser(ownerById);
    }

    @Override
    public void userDeleteListedItem(String data) {
        JSONObject jsonObject = new JSONObject(data);
        User sellerUser = userRepository.getUserById(jsonObject.getString("userId"));
        sellerUser.setItemListedOnMerchantStatus(jsonObject.getString("itemId"), true);
    }

    @Override
    public void priceDecreasedNotification(String data) {
        JSONObject jsonObject = new JSONObject(data);
        List<User> usersHaveTheGameInWishList= userRepository.getUsersHaveTheGameInWishList(jsonObject.getString("gameId"));
        for (User user:usersHaveTheGameInWishList) {
            //sendEmail(user.getEmail(),price,message)
        }
    }
}
