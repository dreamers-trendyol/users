package com.dreamerstrendyol.users.Services;

import com.dreamerstrendyol.users.Contracts.Requests.AddGameDTO;
import com.dreamerstrendyol.users.Contracts.Requests.ChangePasswordDTO;
import com.dreamerstrendyol.users.Contracts.Requests.CreateUserDTO;
import com.dreamerstrendyol.users.Contracts.Responses.AccountDetailDTO;
import com.dreamerstrendyol.users.Models.*;
import com.github.fge.jsonpatch.JsonPatch;

import java.util.List;

public interface UserService {
    String createUser(CreateUserDTO createUserDTO);
    User getUserById(String userId);
    List<UserComment> getUserProfileComments(String userId);
    void addProfileCommentToUser(String userId, UserComment userComment);
    void deleteProfileCommentToUser(String userId, UserComment userComment);
    List<Friend> getUserFriends(String userId);
    void addFriendToUser(String userId, Friend friend);
    void deleteFriendFromUser(String userId, Friend friend);
    double getUserBalance(String userId);
    void increaseBalanceOfUser(String userId, double theBalanceWillAdded);
    void decreaseBalanceOfUser(String userId, double theBalanceWillRemoved);
    AccountDetailDTO getUserAccountDetails(String userId);
    void resetPassword(String userId, String email);
    void changePassword(String userId, ChangePasswordDTO changePasswordDTO);
    void updateUser(String userId, JsonPatch patch);
    List<Game> getUserGames(String userId);
    void userJoinedGame(String userId, String gameId);
    void userExitGame(String userId, String gameId);
    Game getUserGameById(String userId, String gameId);
    void addGameToUser(String userId, AddGameDTO addGameDTO);
    void deleteGameFromUser(String userId, String gameId);
    List<MediaItem> getUserMedias(String userId);
    void addUserMedia(String userId, String mediaUrl);
    void deleteUserMedia(String userId, String mediaUrl);
    List<Item> getUserInventory(String userId);
    Item getUserItemById(String userId, String itemId);
    void addItemToUserInventory(String userId, Item item);
    void deleteItemFromUser(String userId, String itemId);
    void addWishToWishList(String userId, String gameId);
    void deleteWishToWishList(String userId, String gameId);
    List<User> getAllUsers(int offset, int limit);
}
