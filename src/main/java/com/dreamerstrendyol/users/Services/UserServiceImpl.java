package com.dreamerstrendyol.users.Services;

import com.dreamerstrendyol.users.Contracts.Requests.AddGameDTO;
import com.dreamerstrendyol.users.Contracts.Requests.ChangePasswordDTO;
import com.dreamerstrendyol.users.Contracts.Requests.CreateUserDTO;
import com.dreamerstrendyol.users.Contracts.Responses.AccountDetailDTO;
import com.dreamerstrendyol.users.Helpers.PatchHelper;
import com.dreamerstrendyol.users.Models.*;
import com.dreamerstrendyol.users.Repositories.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import lombok.AccessLevel;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    @Setter(AccessLevel.PACKAGE)
    UserRepository userRepository;

    List<playTimeCounter> playTimeCounterList=new ArrayList<>();

    @Override
    public String createUser(CreateUserDTO createUserDTO) {
        User user = new User(createUserDTO.getUsername(), createUserDTO.getPassword(), createUserDTO.getEmail(),
                createUserDTO.getAvatarUrl(), createUserDTO.getAbout());
        userRepository.createUser(user);
        return user.getUserId();
    }

    @Override
    public User getUserById(String userId) {
        return userRepository.getUserById(userId);
    }

    @Override
    public List<UserComment> getUserProfileComments(String userId) {
        User userById = userRepository.getUserById(userId);
        return userById.getProfileComments();
    }

    @Override
    public void addProfileCommentToUser(String userId, UserComment userComment) {
        User userById = userRepository.getUserById(userId);
        userById.addProfileComment(userComment);
        userRepository.updateUser(userById);
    }

    @Override
    public void deleteProfileCommentToUser(String userId, UserComment userComment) {
        User userById = userRepository.getUserById(userId);
        userById.deleteProfileComment(userComment);
        userRepository.updateUser(userById);
    }

    @Override
    public List<Friend> getUserFriends(String userId) {
        return userRepository.getUserFriends(userId);
    }

    @Override
    public void addFriendToUser(String userId, Friend friend) {
        User userById = userRepository.getUserById(userId);
        userById.addFriend(friend);

        User userAsFriend = userRepository.getUserById(friend.getUserId());
        userAsFriend.addFriend(friend);

        userRepository.updateUser(userById);
        userRepository.updateUser(userAsFriend);
    }

    @Override
    public void deleteFriendFromUser(String userId, Friend friend) {
        User userById = userRepository.getUserById(userId);
        userById.deleteFriend(friend);
        userRepository.updateUser(userById);
    }

    @Override
    public double getUserBalance(String userId) {
        User userById = userRepository.getUserById(userId);
        return userById.getBalance();
    }

    @Override
    public void increaseBalanceOfUser(String userId, double theBalanceWillAdded) {
        User userById = userRepository.getUserById(userId);
        userById.increaseBalance(theBalanceWillAdded);
        userRepository.updateUser(userById);
    }

    @Override
    public void decreaseBalanceOfUser(String userId, double theBalanceWillRemoved) {
        User userById = userRepository.getUserById(userId);
        userById.decreaseBalance(theBalanceWillRemoved);
        userRepository.updateUser(userById);
    }

    @Override
    public AccountDetailDTO getUserAccountDetails(String userId) {
        User userById = userRepository.getUserById(userId);
        return new AccountDetailDTO(userById.getUsername(), userById.getEmail(), userById.getAvatarUrl(), userById.getAbout());
    }

    @Override
    public void resetPassword(String userId, String email) {
        User userById = userRepository.getUserById(userId);
        if (userById.getEmail().equals(email)) {
            //sendEmail
        } else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public void changePassword(String userId, ChangePasswordDTO changePasswordDTO) {
        User userById = userRepository.getUserById(userId);
        if (userById.getEmail().equals(changePasswordDTO.getEmail())
                && userById.getPassword().equals(changePasswordDTO.getOldPassword())) {
            userById.setPassword(changePasswordDTO.getNewPassword());
        }
    }

    @Override
    public List<Game> getUserGames(String userId) {
        User userById = userRepository.getUserById(userId);
        return userById.getGames();
    }

    @Override
    public void userJoinedGame(String userId, String gameId) {
        User userById = userRepository.getUserById(userId);
        Game userGameById = userById.getUserGameById(gameId);
        userGameById.updateGameLastTimePlayed();
        userRepository.updateUser(userById);
        this.playTimeCounterList.add(new playTimeCounter(userId, gameId));
    }

    @Override
    public void userExitGame(String userId, String gameId) {
        playTimeCounter playTimeCounter = this.playTimeCounterList.stream()
                .filter(g -> g.getGameId().equals(gameId))
                .findFirst()
                .get();
        int playTime = playTimeCounter.getPlayTime();
        playTimeCounter.kill();
        User userById = userRepository.getUserById(userId);
        Game gameById = userById.getUserGameById(gameId);
        gameById.increaseInGameMinutes(playTime);
        userRepository.updateUser(userById);
    }

    @Override
    public Game getUserGameById(String userId, String gameId) {
        User userById = userRepository.getUserById(userId);
        return userById.getUserGameById(gameId);
    }

    @Override
    public void updateUser(String userId, JsonPatch patch) {
        User userById = userRepository.getUserById(userId);
        try {
            User patchedUser = PatchHelper.applyPatch(patch, userById);
            userRepository.updateUser(patchedUser);
        } catch (JsonPatchException | JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addGameToUser(String userId, AddGameDTO addGameDTO) {
        User userById = userRepository.getUserById(userId);
        userById.addGameToUser(addGameDTO.getGameId(), addGameDTO.getGameName());
        userRepository.updateUser(userById);
    }

    @Override
    public void deleteGameFromUser(String userId, String gameId) {
        User userById = userRepository.getUserById(userId);
        userById.deleteGame(gameId);
        userRepository.updateUser(userById);
    }

    @Override
    public List<MediaItem> getUserMedias(String userId) {
        User userById = userRepository.getUserById(userId);
        return userById.getMedias();
    }

    @Override
    public void addUserMedia(String userId, String mediaUrl) {
        User userById = userRepository.getUserById(userId);
        userById.addMedia(mediaUrl);
        userRepository.updateUser(userById);
    }

    @Override
    public void deleteUserMedia(String userId, String mediaUrl) {
        User userById = userRepository.getUserById(userId);
        userById.deleteMedia(mediaUrl);
        userRepository.updateUser(userById);
    }

    @Override
    public List<Item> getUserInventory(String userId) {
        User userById = userRepository.getUserById(userId);
        return userById.getInventory();
    }

    @Override
    public Item getUserItemById(String userId, String itemId) {
        User userById = userRepository.getUserById(userId);
        return userById.getItem(itemId);

    }

    @Override
    public void addItemToUserInventory(String userId, Item item) {
        User userById = userRepository.getUserById(userId);
        userById.addItemToInventory(item);
        userRepository.updateUser(userById);
    }

    @Override
    public void deleteItemFromUser(String userId, String itemId) {
        User userById = userRepository.getUserById(userId);
        userById.deleteItemToInventory(itemId);
        userRepository.updateUser(userById);
    }

    @Override
    public void addWishToWishList(String userId, String gameId) {
        User userById = userRepository.getUserById(userId);
        userById.addWishToList(gameId);
        userRepository.updateUser(userById);
    }

    @Override
    public void deleteWishToWishList(String userId, String gameId) {
        User userById = userRepository.getUserById(userId);
        userById.deleteWishFromList(gameId);
        userRepository.updateUser(userById);
    }

    @Override
    public List<User> getAllUsers(int offset, int limit) {
        return userRepository.getAllUsers(offset, limit);

    }
}
